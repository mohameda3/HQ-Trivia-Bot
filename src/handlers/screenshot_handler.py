from watchdog.events import FileSystemEventHandler

from os.path import expanduser
import os

PATH = path = expanduser("~") + "/Desktop/"
DESTINATION = os.path.abspath(os.curdir) + "/src/resources/question.png"


class ScreenshotEventHandler(FileSystemEventHandler):
    '''
    An file event handler that is triggered by screenshot
    '''
    def __init__(self, observer):
        self.observer = observer

    def on_created(self, event):
        '''
        Stop observing if a screenshot occurs, and rename/move the file to the appropriate folder
        so it can be processed.
        :param event: FileSystemEvent
        :return:
        '''
        basename = os.path.basename(event.src_path)
        if not event.is_directory and "Screen Shot" in basename:
            os.rename(event.src_path, DESTINATION)
            self.observer.stop()
