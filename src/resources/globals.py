
# Languages
LANG_SHORT = "en"
LANG_MEDIUM = "eng"
LANG_FULL = "english"

# OpenCV
THRESH_PATH = "test/resources/screens/thresh.png"
WHITE_SCREEN = "test/resources/screens/white-screen.png"
ANSWER_SCREEN = "test/resources/screens/answer-partial.png"

# Project configs
VERSION = "1.0.0"
TEST_GAME_LENGTH = 30
SEARCH_PAGE_LIMIT = 5
NEGATION = False
GOOGLE_CONFIG = "src/resources/gsearch_config.json"

# File Paths
TEST_GAME_PATH = "test/resources/questions/questions-superset.json"
TEST_GAME_SMALL_PATH = "test/resources/questions/sample-game.json"
TEST_NOT_GAME_SMALL_PATH = "test/resources/questions/sample-negative-game.json"
