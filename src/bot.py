from src.entities.game import Game
from src.handlers.screenshot_handler import ScreenshotEventHandler
from src.preprocessors.question_preprocessor import QuestionPreprocessor
from src.preprocessors.image_preprocessor import ImagePreprocessor
from src.agents.google_cse_basic import GoogleCSEBasicSearch
from src.agents.wiki import WikipediaAgent
from src.resources import globals

import argparse
import json
import os
from watchdog.observers import Observer

PATH = os.path.expanduser("~") + "/Desktop/"
DESTINATION = os.path.abspath(os.curdir) + "/src/resources/question.png"

with open(globals.GOOGLE_CONFIG) as file:
    GOOG_CONFIG = json.loads(file.read())
GOOG_KEY = GOOG_CONFIG.get("key")
GOOG_CSE = GOOG_CONFIG.get("cx")

def initialize_arg_parser():
    '''

    :return:
    '''
    parser = argparse.ArgumentParser(usage="TBI")
    parser.add_argument("-t", "--test", required=False, action="store_true", help="Test mode - Load game from local file.")
    parser.add_argument("-X", "--debug", required=False, action="store_true", help="Produce execution debug output")
    search_group = parser.add_mutually_exclusive_group(required=True)
    search_group.add_argument("-g", "--google", required=False, action="store_true", help="Use Wikipedia as search agent.")
    search_group.add_argument("-w", "--wiki", required=False, action="store_true", help="Use Google Custom Search Engine as search agent.")
    return parser.parse_args()


if __name__ == "__main__":
    args = initialize_arg_parser()
    # Initialize our processors/solvers
    if args.google:
        solver = GoogleCSEBasicSearch(GOOG_KEY, GOOG_CSE)
    else:
        solver = WikipediaAgent()
    # This is what we'll use to optimize our question string for searching
    qp = QuestionPreprocessor(args)
    # Test mode, load question(s) in from file
    if args.test:
        # Create a game object (list of questions)
        file = open(globals.TEST_GAME_PATH, "r")
        jsonDict = json.loads(file.read())
        game = (Game(jsonDict))
        file.close()
        correct_count = 0
        SIZE = globals.TEST_GAME_LENGTH if globals.TEST_GAME_LENGTH < game.get_size() else game.get_size()
        for i in range(0, SIZE):
            # Get all the relevant information for each Question object
            question_full = game.get_random_question()
            question = question_full.get_question()
            answer_options_list = question_full.get_answer_options()
            # Optimize the question string for search
            negated, question_processed, answer_options_list_processed = qp.preprocessing(question, answer_options_list)
            question_full.set_processed_question(question_processed)
            question_full.set_processed_answer_options(answer_options_list_processed)
            print(question_full)
            index, scores = solver.predict(question_full, negated)
            print("Predicted index is {0}".format(index))
            print(scores)
            print("---------------------------------------------------------------------------------------------------")
            if index == question_full.get_right_answer_index():
                correct_count += 1
            print("Got {0} out of {1} questions correct so far".format(correct_count, i+1))
            print("---------------------------------------------------------------------------------------------------")

    else:
        # Watch the file system for screenshots
        observer = Observer()
        handler = ScreenshotEventHandler(observer)
        observer.schedule(handler, PATH, recursive=False)
        observer.start()
        observer.join()
        # Load question in from image
        ip = ImagePreprocessor(args)
        ocr_output = ip.question_screenshot_conversion_tesseract(DESTINATION)
        question, answer_options_list = ip.question_parsing_tesseract(ocr_output)
        # This is what we'll use to optimize our question string for searching
