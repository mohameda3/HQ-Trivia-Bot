import cv2
import imutils
import os
import pytesseract
import re

from src.resources import globals
from PIL import Image

LANG = "eng"
LANG_FULL = "english"
THRESH_PATH = "test/resources/screens/thresh.png"
WHITE_SCREEN = "test/resources/screens/white-screen.png"
ANSWER_SCREEN = "test/resources/screens/answer-partial.png"

class ImagePreprocessor:
    '''
    A class that processes an Image into text using OpenCV and Tesseract
    '''

    def __init__(self, args):
        self.args = args

    def question_parsing_tesseract(self, output):
        '''
        Take the output of tesseract and turn it into meaningful strings
        :param output: string result of ocr
        :return: a question string and a list of answer strings
        '''

        question_index = output.find("?")
        question = output[0:question_index + 1].replace("\n", " ")
        answers_raw = output[question_index + 1:].split("\n")
        answers_list = list(filter(None, answers_raw))

        return question, answers_list

    def question_screenshot_conversion_tesseract(self, filepath):
        '''
        Take a screenshot png and use OpenCV and Tesseract to
        turn that into text.
        :param filepath: A filepath to the screenshotted image
        :return: an output string from pytesseract
        '''

        # Load the image
        image = cv2.imread(filepath)
        try:
            resized = imutils.resize(image, 400)

            # Convert all colour into black/white
            gray = cv2.cvtColor(resized, cv2.COLOR_BGR2GRAY)
            retval, thresh = cv2.threshold(gray, 125, 255, cv2.THRESH_BINARY)

            # Save, ocr, then delete the image
            cv2.imwrite(THRESH_PATH, thresh)
            ocr_output = pytesseract.image_to_string(Image.open(globals.THRESH_PATH), globals.LANG_MEDIUM)
            os.remove(THRESH_PATH)

            self.validate_tesseract(ocr_output)
            print(ocr_output)
            print(ocr_output.count("\n"))
            return ocr_output

        except (AttributeError, ValueError) as e:
            if isinstance(e, AttributeError):
                print("\33[91m[OpenCV Error] There was a problem when converting the image. \33[0m")
            exit(1)

    def validate_tesseract(self, output):
        '''
        Throw value error if tesseract returns junk.
        Fall back on google vision API if something goes wrong here?
        :param output: an output string from pytesseract
        '''

        # TO-DO: Create a better way to check if tesseract correctly ocr'd the text

        if "?" not in output or output.count("\n") < 7:
            print("\33[91m[Tesseract Error] There was a problem when performing OCR. Here is the result.....\n"
                  "{0} \33[0m".format(output))
            raise ValueError
