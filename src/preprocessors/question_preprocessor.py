import inflect

from nltk.corpus import stopwords
from nltk.stem import PorterStemmer
from nltk.stem import SnowballStemmer
from nltk.stem import WordNetLemmatizer
from src.resources import globals


class QuestionPreprocessor:
    '''
    A class the preprocesses a question using NLTK methods such as stemming, lemmatization, removing stopwords,
    lowercasing, inflection, and more (to be implemented) in order to optimize search.
    '''

    def __init__(self, args=None) -> None:
        '''

        :param args:
        '''
        self.NEGATED_WORDS = []
        self.STOP_WORDS = set(stopwords.words(globals.LANG_FULL))
        self.SNOW = SnowballStemmer(globals.LANG_FULL)
        self.PORTER = PorterStemmer()
        # lemma = WordNetLemmatizer()
        self.inf = inflect.engine()
        self.args = args

        with open("src/resources/negative-words.txt") as f:
            self.NEGATED_WORDS = f.read().split("\n")
        f.close()

    def preprocessing(self, question, answer_options_list):
        '''
        Preprocess the question using various methods
        :param question:
        :param answer_options_list:
        :return: processed version question, answer_options_list
        '''
        question_lower, answer_options_list_lower = self.lowercase_preprocessing(question, answer_options_list)
        negated, question_negated = self.contains_negation(question_lower)
        question_nltk = self.nltk_preprocessing(question_negated)
        question_singular = self.singular_preprocessing(question_nltk)
        return negated, question_singular.replace("?", "").replace(",", "").replace("&amp;", "&"), answer_options_list_lower

    def nltk_preprocessing(self, question):
        '''
        Cleanup the question by removing stopwords and stemming
        to their originators
        :param question:
        :return: processed version question, answer_options_list
        '''
        # Remove all stop words
        question_list = question.replace("?", "").split()
        self.contains_negation(question)
        filtered_list = [word for word in question_list if word not in self.STOP_WORDS]

        # # Stem all words

        # TO-DO: Determine if there is a proper way that stemming will increase search correctness

        # porter_list = [PORTER.stem(word) for word in filtered_list]
        # snow_list = [SNOW.stem(word) for word in filtered_list]
        # # lemma_list = [lemma.lemmatize(word) for word in filtered_list]
        #
        # question_filtered = " ".join(filtered_list)
        # question_porter = " ".join(porter_list)
        # question_snow = " ".join(snow_list)
        # # question_lemma = " ".join(lemma_list)
        #
        # print("ORIGINAL: \t" + question)
        # print("FILTERED: \t" + question_filtered)
        # print("PORTER: \t" + question_porter)
        # print("SNOW: \t" + question_snow)
        # # print("LEMMA: \t" + question_lemma)

        question_processed = " ".join(filtered_list) + "?"

        if self.args.debug:
            print("ORIG : \t " + question)
            print("NLTK : \t " + question_processed)

        return question_processed

    def singular_preprocessing(self, question):
        '''
        Cleanup the question and answer by making things singular
        :param question:
        :return: processed version question, answer_options_list
        '''

        question_list = question.replace("?", "").split()
        question_processed_list = []

        # Turn plural nouns in the question to singular
        for word in question_list:
            res = self.inf.singular_noun(word)
            if not res:
                question_processed_list.append(word)
            else:
                question_processed_list.append(res)

        question_processed = " ".join(question_processed_list) + "?"

        if self.args.debug:
            print("SING : \t " + question_processed)

        return question_processed

    def lowercase_preprocessing(self,question, answers_list):
        '''
        Cleanup the question and answer by making things lowercase
        :param question:
        :param answers_list:
        :return: processed version question, answer_options_list
        '''

        question_processed = str.lower(question)
        answers_options_processed = [str.lower(answer) for answer in answers_list]

        return question_processed, answers_options_processed

    def contains_negation(self, question):
        '''
        Checks the question to see if it contains a negation and removes those words
        :param question:
        :return: boolean depending on whether the question contains a negation and question without negated words
        '''
        res = False

        # TO-DO: Make sure to ignite negated words that are quoted!

        question_list = question.replace("?", "").split()
        filtered_list = []
        for word in question_list:
            if word not in self.NEGATED_WORDS:
                filtered_list.append(word)
            else:
                res = True

        question_processed = " ".join(filtered_list) + "?"
        return res, question_processed


    def contains_quote(self, question):
        '''
        Checks the question to see if there are any quoted regions
        :param question:
        :return: booea
        '''
        raise NotImplementedError



