import json

class Question:
    '''
    A class that encapsulates a 3 option HQ Trivia question
    '''

    def __init__(self, jsonDict)-> None:
        '''
        Take in a json object and create a Question object
        :param jsonObject:
        '''
        question = jsonDict.get("question")
        solution = jsonDict.get("solution")

        self.question = question.get("question")
        self.answer1 = question.get("answer1")
        self.answer2 = question.get("answer2")
        self.answer3 = question.get("answer3")
        self.processed_question = ""
        self.processed_answer_options = []

        # Solution may or may not be specified (live game, solutions are not present when creating a question)
        if solution is None:
            self.right_answer = self.right_answer_index = None
        else:
            self.right_answer = solution.get("rightAnswer")
            self.right_answer_index = solution.get("rightAnswerIndex")

    def export(self) -> str:
        '''

        :return:
        '''

        completeQuestion = {}
        question = {}
        solution = {}

        question["question"] = self.question
        question["answer1"] = self.answer1
        question["answer2"] = self.answer2
        question["answer3"] = self.answer3
        completeQuestion["question"] = question

        if self.right_answer is not None and self.right_answer_index is not None:
            solution["rightAnswer"] = self.right_answer
            solution["rightAnswerIndex"] = self.right_answer
            completeQuestion["solution"] = solution

        return json.dumps(completeQuestion, indent=4)

    def get_complete(self) -> bool:
        '''

        :return:
        '''
        return self.right_answer is not None and self.right_answer_index is not None

    def get_question(self) -> str:
        '''

        :return:
        '''
        return self.question

    def get_answer_options(self) -> list:
        '''

        :return:
        '''
        return [self.answer1, self.answer2, self.answer3]

    def get_right_answer(self) -> str:
        '''

        :return:
        '''
        return self.right_answer

    def get_right_answer_index(self) -> str:
        '''

        :return:
        '''
        return self.right_answer_index

    def get_processed_question(self) -> str:
        '''

        :return:
        '''
        return self.processed_question

    def get_processed_answer_options(self) -> []:
        '''

        :return:
        '''
        return self.processed_answer_options

    def set_right_answer_index(self, index) -> None:
        '''

        :return:
        '''
        self.right_answer_index = index

    def set_right_answer(self, answer) -> None:
        '''

        :return:
        '''
        self.right_answer = answer

    def set_processed_question(self, processed_question) -> None:
        '''

        :return:
        '''
        self.processed_question = processed_question

    def set_processed_answer_options(self, processed_answer_options) -> None:
        '''

        :return:
        '''
        self.processed_answer_options = processed_answer_options

    def __str__(self) -> str:
        '''

        :return:
        '''
        return "Question: {0} \n" \
               "{1}\n" \
               "{2}\n" \
               "{3}\n" \
               "Answer is: {4}\n" \
               "Answer index is: {5}\n" \
               "Processed Question: {6}".format(self.question, self.answer1, self.answer2, self.answer3, self.right_answer,
                                             self.right_answer_index, self.get_processed_question())

