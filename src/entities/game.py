import random
from src.entities.question import Question


class Game:
    '''
    A set of Question objects used primarily for exporting/loading in test sets.
    '''
    def __init__(self, jsonDictList=None) -> None:
        '''
        Take in a json object and create a Game object
        :param jsonObject:
        '''

        self.questionList = []
        if jsonDictList is not None:
            for blob in jsonDictList:
                question = Question(blob)
                self.questionList.append(question)

    def export(self) -> str:
        '''

        :return:
        '''

        out = "["
        for i in range(0,len(self.questionList)):
            question = self.questionList[i]
            out += question.export()

            if i != len(self.questionList)-1:
                out += ",\n"

        out += "]"
        return out

    def add_question(self, question) -> None:
        '''

        :param question:
        :return:
        '''

        self.questionList.append(question)

    def get_question(self) -> object:
        '''
        :return:
        '''

        if len(self.questionList) == 0:
            return None
        return self.questionList.pop(0)

    def get_random_question(self):
        '''

        :return:
        '''

        if len(self.questionList) == 0:
            return None
        index = random.randint(0,len(self.questionList)-1)
        return self.questionList.pop(index)

    def get_size(self) -> int:
        '''

        :return:
        '''

        return len(self.questionList)

    def __str__(self) -> str:
        '''

        :return:
        '''
        out = "Game size: {0}\n" \
              "Questions list:\n".format(len(self.questionList))

        for question in self.questionList:
            out += '\n' + question.__str__()

        out += "\n\nGame done.."

        return out

