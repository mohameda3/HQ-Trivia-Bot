from googleapiclient.discovery import build
from src.agents.agent import Agent

class GoogleCSEBasicSearch(Agent):
    '''
    A search agent that uses Google Custom Search to find the most suitable answer
    for a question
    '''

    def __init__(self, google_api_key, google_cse_key, debug=False):
        super().__init__()
        self.google_api_key = google_api_key
        self.google_cse_key = google_cse_key

    def predict(self, question, negation=False):
        scores = self.solve_async(question, negation)
        predicted_index = 0
        predicted_value = scores[0]
        if negation:
            predicted_index = scores.index(max(scores))
        else:
            # Find min non zero index
            for i in range(1, len(scores)):
                if 0 < scores[i] < predicted_value or predicted_value == 0:
                    predicted_index = i
                    predicted_value = scores[i]

        return predicted_index+1, scores

    def solve_async(self, question, negation=False):
        answers = question.get_processed_answer_options()
        async_answer_1 = self.pool.apply_async(self.google_cse_search, (question.get_processed_question() + " " + answers[0],))
        async_answer_2 = self.pool.apply_async(self.google_cse_search, (question.get_processed_question() + " " + answers[1],))
        async_answer_3 = self.pool.apply_async(self.google_cse_search, (question.get_processed_question() + " " + answers[2],))
        return [async_answer_1.get(), async_answer_2.get(), async_answer_3.get()]

    def solve_sync(self, question, negation=False):
        scores = []
        for answer in question.get_processed_answer_options():
            query = question.get_processed_question() + " " + answer
            scores.append(self.google_cse_search(query))
        return scores

    def google_cse_search(self, query):
        '''
        Search Google using CSE API to find the most suitable answer.
        :param query: A question + answer string that will be google searched
        :return: A rating for that search
        '''
        service = build("customsearch", "v1", developerKey=self.google_api_key)
        try:
            response = service.cse().list(
            q=query,
            cx=self.google_cse_key,
            lr="lang_en",
            filter="1",
            num=5
          ).execute()
            return self.get_search_rating(response)
        except Exception as e:
            print("\33[91m[Google CSE Error] Error when searching for answer. Daily rate limit for key has most likely"
                  "been reached.\33[0m")
            return 0


    def get_search_rating(self, response):
        '''
        Score a query based on the number of search results
        :param response:
        :return: int search results
        '''
        return int(response["searchInformation"]["totalResults"])
