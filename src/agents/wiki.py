import wikipedia

from collections import Counter
from src.resources import globals
from src.agents.agent import Agent


class WikipediaAgent(Agent):

    def __init__(self, debug=False):
        super().__init__()
        wikipedia.set_lang(globals.LANG_SHORT)

    def predict(self, question, negation=False):
        scores = self.solve_async(question, negation)
        predicted_index = 0
        predicted_value = scores[0]

        # Get the index with the higher score
        if not negation:
            predicted_index = scores.index(max(scores))
        else:
            # Find min non zero index
            for i in range(1, len(scores)):
                if 0 < scores[i] < predicted_value or predicted_value == 0:
                    predicted_index = i
                    predicted_value = scores[i]

        return predicted_index+1, scores

    def solve_async(self, question, negation=False):
        async_answer_1 = self.pool.apply_async(self.wikipedia_search, (question.get_answer_options()[0], question))
        async_answer_2 = self.pool.apply_async(self.wikipedia_search, (question.get_answer_options()[1], question))
        async_answer_3 = self.pool.apply_async(self.wikipedia_search, (question.get_answer_options()[2], question))
        return [async_answer_1.get(), async_answer_2.get(), async_answer_3.get()]

    def solve_sync(self, question, negation=False):
        scores = []
        for answer in question.get_processed_answer_options():
            scores.append(self.wikipedia_search(answer, question))
        return scores

    def wikipedia_search(self, answer, question):
        '''
        Search Google using CSE API to find the most suitable answer.
        :param query: A question + answer string that will be google searched
        :return: A rating for that search
        '''
        try:
            response = wikipedia.search(answer, 1)
            if len(response) == 0:
                return 0
            page = wikipedia.page(response[0])
            return self.get_search_rating(page, question)
        except wikipedia.DisambiguationError as e:
            print("\33[91m[Wikipedia Error] There was a problem when searching for the correct answer. "
                  "Search query is too disambiguous \33[0m")
            return 0
        except wikipedia.PageError as e:
            print("\33[91m[Wikipedia Error] There was a problem when searching for the correct answer. "
                  "Page id does not match any pages\33[0m")
            return 0

    def get_search_rating(self, page, question):
        '''
        Score a query based on the number of occurences of the processed question on this wikipedia page.
        :param page: A wikipedia page representing the answer
        :return: int search results
        '''
        score = 0
        key_words = question.get_processed_question().split()
        word_counter = Counter(page.content.lower().split())
        for word in key_words:
            occurences = word_counter[word]
            score += occurences
        return score
