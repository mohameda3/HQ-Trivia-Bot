from multiprocessing.pool import ThreadPool

class Agent:
    '''
    A search agent super class
    '''

    def __init__(self, debug=False):
        self.debug = debug
        self.pool = ThreadPool(3)

    def predict(self, question, negation=False):
        '''
        Predict the most probable answer to a question
        :param question: A question object that has been preprocessed
        :param negation: A boolean value determining whether there is a NOT present in the question
        :return: Integer, List[Integer]
        '''
        raise NotImplementedError

    def solve_async(self, question, negation=False):
        '''
        Generate scores for each answer synchronously
        :param question:
        :param negation:
        :return:
        '''
        raise NotImplementedError

    def solve_sync(self, question, negation=False):
        '''
        Generate scores for each answer asynchronously
        :param question:
        :param negation:
        :return:
        '''
        raise NotImplementedError
