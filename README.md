# HQ-Trivia-Bot

This bot helps solve the trivia game `HQ`. 

Currently, it will be triggered by taking a screenshot of a question and using 
Google Tesseract's OCR to transform that image in to text. 

Next, it automates the search by using Google Custom Search Engine and Wikipedia to predict an answer.

## Getting Started

These instructions will get you a copy of the project up and running on your 
local machine. Currently (and for the forseeable future), only Mac OSX is supported.

Some prerequisites are listed below.

* python 3.7+
* Quicktime Player (for mirroring screen)


### Installing

Below are a list of all packages used in the development of this version.

* google-api-python-client - Client library for accessing the Google CSE
* wikipedia - Library that makes it easy to access and parse data from Wikipedia
* pytest - Framework makes it easy to write small tests,
* pytesseract - An optical character recognition (OCR) tool for python
* opencv-python - A computer vision image processing tool
* imutils - A series of convenience functions to make basic image processing easier
* Pillow - Imaging library
* inflect - Correctly generate plurals, singular nouns, ordinals, indefinite articles
* nltk - Natural Language Toolkit for processing
* watchdog - Monitor file system events

Install the above packages.
* Run `brew install tesseract`
* Run `brew install opencv`
* Run `pip3 install -r requirements.txt` in the project directory

## Usage

Follow this [tutorial](https://www.youtube.com/watch?v=g8yi9bJtkLY) on how to 
get started mirroring your device.

Next, run `python3 src/bot.py --google/--wiki` in the project directory 
(one of this commands is mandatory) with the solver of your choice. 
Everytime a question appears on screen, take the smallest screenshot of ONLY just the question and answer. 
This will make Tesseract job much easier and more accurate when performing character recognition.

The answer should appear with a couple of seconds at most, or an 
error message will be printed to the terminal. You should still have enough 
time to choose an answer even if the solvers fail.

## Versioning

These solvers are far from perfect, and much work is still left to be done.

To see a more in detail look about current pitfalls, accuracy percentages, and future paths, take a look at the [current version](https://gitlab.com/mohameda3/HQ-Trivia-Bot/wikis/Version-1.0.0).

## Contributing

Currently no plans for contribution.

## License

This project is licensed under the MIT License - see the [LICENSE.md](LICENSE) file for details

## Acknowledgments

* [Jake Mor's Medium article](https://medium.com/@jakemor/hquack-my-public-hq-trivia-bot-is-shutting-down-5d9fcdbc9f6e)
* [Tony Mellor's Medium article](https://medium.com/@tobymellor/hq-trivia-using-bots-to-win-money-from-online-game-shows-ce2a1b11828b)
