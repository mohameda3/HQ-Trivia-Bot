import json
import unittest
from src.entities.question import Question

QUESTION_COMPLETE = 'test/resources/questions/' \
             'sample-question.json'

class PreprocessingTest(unittest.TestCase):
    '''
    Test class for question.py
    '''

    def test_basic_preprocessing(self):

        file = open(QUESTION_COMPLETE, "r")
        jsonDict = json.loads(file.read())
        q = Question(jsonDict)

        # lower_question, lower_answerList = basic_preprocessing(q.getQuestion(), q.getAnswerOptions())
        # self.assertEquals(lower_question, str.lower(q.getQuestion()))
        #
        # answerList = [str.lower(answer) for answer in q.getAnswerOptions()]
        # self.assertEquals(lower_answerList, answerList)

