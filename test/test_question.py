import json
import unittest
from src.entities.question import Question


QUESTION_INCOMPLETE = 'test/resources/questions/' \
            'sample-question-incomplete.json'
QUESTION_COMPLETE = 'test/resources/questions/' \
             'sample-question.json'


class QuestionTest(unittest.TestCase):
    '''
    Test class for question.py
    '''

    def test_export(self):
        '''

        :return:
        '''
        pass

    def test_completeQuestionTest(self):
        '''
        Check to see if a complete question object is marked as complete
        :return:
        '''
        file = open(QUESTION_COMPLETE, "r")
        jsonDict = json.loads(file.read())
        question = (Question(jsonDict))

        self.assertTrue(question.get_complete())

        file.close()

    def test_incompleteQuestionTest(self):
        '''
        Check to see if an incomplete question object becomes complete by adding it's missing parameters
        :return:
        '''

        file = open(QUESTION_INCOMPLETE, "r")
        jsonDict = json.loads(file.read())
        question = (Question(jsonDict))

        self.assertFalse(question.get_complete())
        question.set_right_answer_index(2)
        self.assertFalse(question.get_complete())
        question.set_right_answer(question.get_answer_options()[1])
        self.assertTrue(question.get_complete())

        file.close()


if __name__ == '__main__':
    unittest.main()

